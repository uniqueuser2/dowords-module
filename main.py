from flask import Flask, render_template, request, redirect, url_for, session

app = Flask(__name__)
app.secret_key = "secretkey"


#create data
users = {"user1": "abc", "user2": "abc", "user3": "abc"}

@app.route("/import")
def imp():
    CSVname = request.form["file name"]
    with open(CSVname, "r") as csvdata:
        while csvdata.readline() != "":
            enclosed = False
            word = ""
            meaning = ""
            sound = ""
            examples = ""
            extras = ""

            for line in csvdata:
                counter = 1
                for char in csvdata.read(len(csvdata.readline())):
                    if char == ",":
                        if enclosed:
                            continue
                        else:
                            counter += 1
                    if char == "\"":
                        enclosed = not enclosed
                    if char == "":
                        pass
                    if (enclosed == True):
                        if counter == 1:
                            word = word + char
                        if counter == 2:
                            meaning = meaning + char
                        if counter == 3:
                            sound == sound + char
                        if counter == 4:
                            examples = examples + char
                        if counter == 5:
                            extras = extras + char
            # instert into statement here


    return redirect(url_for('home'))


@app.route("/")
def home():
    if "username" in session:
        return render_template("home.html", username = session("username"))
    else:
        return redirect(url_for("login"))
    while True:
        CSVname = request.form["file name"]
        with open(CSVname, "r") as csvdata:
            while csvdata.readline() != "":
                enclosed = False
                word = ""
                meaning = ""
                sound = ""
                examples = ""
                extras = ""

                for line in csvdata:
                    counter = 1
                    for char in csvdata.read(len(csvdata.readline())):
                        if char == ",":
                            if enclosed:
                                continue
                            else:
                                counter += 1
                        if char == "\"":
                            enclosed = not enclosed
                        if char == "":
                            pass
                        if(enclosed == True ):
                            if counter == 1:
                                word = word + char
                            if counter == 2:
                                meaning = meaning + char
                            if counter == 3:
                                sound == sound + char
                            if counter == 4:
                                examples = examples + char
                            if counter == 5:
                                extras = extras + char
                #instert into statement here
    return("Hello this is the home page")

@app.route("/login", methods=["GET","POST"])
def login():
    if(request.method == "POST"):
        username = request.form["username"]
        password = request.form["password"]
        print(f"username is: {username} and password is {password}")
        if(username in users and users[username] == password):
            return redirect(url_for("home"))
        else:
            return render_template("login.html", message="Invalid username or password")
    else:
        return render_template("login.html")

if __name__ == "__main__":
    app.run(debug = True)